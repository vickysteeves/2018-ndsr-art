# Research Interview {-}
Interviews are one way in which you can get more detail and dig deeper into one individuals experiences or attitudes towards a phenomenon, organization, culture, or space [@nixon]. Interviews are particularly useful for getting the story behind aparticipant's experiences. The interviewer can pursue in-depth information around the topic. Interviews may be useful as follow-up to certain participants to surveys,e.g., to further investigate their responses [@kvale]. This would also allow for a mixed methods approach -- but be careful in that your population may feel your study is eating up too much of their time!

One thing to underscore in all this -- INTERVIEWS ARE NOT IN-PERSON SURVEYS! 

+ Identify topics to discuss, rather than rigid, very specific questions
+ Only use open-ended questions - you want your participants to elaborate, say things in their own words

The stages of your research process should ideally look like this: 

![Design by me, information form @kvale and @thompson](imgs/qualcycle.png)

<img src="imgs/exercise.png" style="float:left; padding-right: 0.5em;" alt="exercise"> **EXERCISE 1 -- 5 minutes.
Write down the larger questions you'll explore during your residency.  Outline the broad areas of knowledge that are relevant to answering these questions.**

## Creating your interview  {-}

Once you've design your overall study, you'll need to decide what kind of interview methodology you want to use, and how to go about creating (or not!) an interview guide. The three most common interview structures are:

1. *Informal, conversational interview*: no predetermined questions are asked, in order to remain as open and adaptable as possible to the interviewee's nature and priorities; during the interview, the interviewer "goes with the flow".
2. *General interview guide approach*: use a guide to ensure that the same general areas of information are collected from each interviewee; this provides more focus than the conversational approach but still allows a degree of freedom and adaptability in getting information from the interviewee.
3. *Standardized, open-ended interview*: the same open-ended questions are asked to all interviewees. This approach facilitates faster interviews that can be more easily analyzed and compared.

[@jacob]

I generally recommend folks use the general interview guide approach, especially for conducting internal environmental scans. This helps newcomes to the method maintain flexibility, but also have a guide to work off of in case of a lull in the conversation, or to check that all the key points were covered in the conversation. Those points typically fall into one of 6 categories of questions to ask about: 

![Interview question categories. Design by me, information apdapted from @macnamara](imgs/categories.png)

When designing your interview guide, you want to pay attention to the sequence of the questions. Are you starting off with the heavy hitters, such as "Do you believe in God?" or are you starting with something a bit more low-key, such as "". Some points to remember about the sequencing include:

+ Get the participants involved in the interview as soon as possible.
+ First ask about some facts. With this approach, participants can more easily engage in the interview before warming up to more personal matters.
    - But not too many in a row! Sprinkle the fact-based questions throughout the interview to avoid long lists of fact-based questions, which tends to leave participants disengaged.
+ Ask questions about the present first. It's usually an easier transition for participants to make, from the present to the past or future.
+ The last questions should always be, "Do you have any questions for me?"

[@macnamara]

You also want to pay attention to the way you are wording the questions. I always make sure that when I write an interview guide, I do it in as much of my own voice as possible. This makes it less awkward for me to read something off the page, or to riff on a question mid-conversation. A few key ideas to keep in mind are:

+ Wording should be *open-ended*. Participants should be able to *choose their own terms* when answering questions.
+ *Avoid wording that might influence answers*, e.g., evocative, judgmental wording.
+ Ask questions *one at a time* to avoid confusion
+ *Be clear*! This includes jargon!
+ Be *careful asking "why"* questions. This type of question infers a cause-effect relationship that may not truly exist. Ask "how" questions instead!
+ *Develop probes* that will elicit more detailed responses. These are basically follow-up questions that can be used to 'probe' the participant for more elaborate answers.
+ Think about the *logical flow* of the interview. What topics should come first?  What  follows more or less "naturally"?  This may take some adjustment after several interviews, and that's ok. Provide yourself with some transition between major topics, e.g., "we've been talking about (some topic) and now I'd like to move on to (another topic)."

[@macnamara]

**ALWAYS use a script for the beginning and end of your interview**

There will be lots of important information that you will want to share with each of your participants, and without a script you are likely to forget something. The script at the beginning should prompt you to share critical details about your study:

+ what you are researching and why
+ informed consent, privacy, confidentiality concerns
+ perhaps a little about yourself, so you can start to build a rapport

At the end, you might want to give the interviewee your contact information, solicit feedback on the process, or other relevant information about following up on the interview. [@jacob]

<img src="imgs/exercise.png" style="float:left; padding-right: 0.5em;" alt="exercise"> **EXERCISE 2 -- 10 minutes. Write out 3 questions (and follow-ups!) that you could ask participants to help you get answers to the questions you've outlined above. Write a 2-3 sentence pre-interview script to read out before asking those questions.**

## Interview Process {-}

**Recruiting** is a big first step of the project. Convincing busy people to take time out of their day to talk you is much harder than you may think. You'll likely be recruiting based on an organizational chart from your mentor, with an accompanying email list. 

When you write your BRIEF email, try to give these details: 

1. Explain who you are (spell out NDSR the first time!), why you want to talk  to them, and the purpose of the interview.  
2. Give a general sense of the sort of questions they will be asked, and approximately how long the interview will last.  
3. Provide some basic information about confidentiality which will put folks at ease and make them more likely to talk to you -- audio recordings will be destroyed after the residency is completed on this date, transcripts will be anonymous, etc. 

If they respond you should:

1. Arrange a time to meet. Pick a fairly public location, but one with few distractions. Most interviewees will feel most comfortable if you interview them at their offices.  
2. Ask to obtain permission in writing to quote the participants (anonymously), to record the session if you will be doing any of these activities. You can email a form for them to sign, or bring it with you to the interview for their signature. This is called *informed consent*, and you can read more about it here:

When recruiting, you want to sample the population in a way that would identify "key informants". There are a few methods you could use to this end:

+ *Convenience sampling* - Find people who are easy to find (which, for a short term project, is not the worst thing)
+ *Snowball sampling* - Find initial relevant subjects, ask them to refer you to more
+ *Quota sampling* - Determine what population looks like in specific qualities then sample a quota from each group

![Quota sampling](imgs/Simple_random_sampling.png)

### Pre-interview {-}

Before you go into your first interview, you should ideally practice with one another, or ideally your mentor. You may end up changing how you ask about topics, where you need to probe, what probes work/don't work in a practice run. 

When you are sitting down with the participants, run through your script. Reiterate the the format of the interview (length, type, etc.). If you want them to ask questions, specify if they're to do so as they have them or wait until the end of the interview. **Don't count on your memory to recall their answers**. Ask for permission to record the interview or to bring along someone to take notes. [@macnamara]

### During the interview {-}

When conducting qualitative interviews, **good rapport is imperative**. Participant will talk candidly only if they:

+ Feel comfortable in the space
+ Trust the interviewer
+ Feel secure about confidentiality
+ Believe the interviewer is interested in their story
+ Do not feel judged

Things to avoid when conducting the interview include:

+ *Influencing responses* by asking leading questions or conveying own view (implicitly or explicitly). Be especially careful about the appearance when note taking. That is, if you jump to take a note, it may appear as if you're surprised or very pleased about an answer, which may influence answers to future questions.
+ *Moving too quickly* from one topic to the next 
+ *Interrupting* the participant
+ Showing *strong emotional reactions* to their responses

[@nixon]

You also need to be willing to make "on the spot" revisions to your interview protocol. Most likely, a question will occur to you that you haven't written down. Sometimes the "ah-ha" question that makes a great project comes to you in the moment. Trust your instincts, ask the question! Just don't get too in the weeds -- that is, don't go off on a tangent. That said, if  you go off book from the interview protocol, you may find something interesting that you did not expect.
[@macnamara]

>  The Resident would start by with the most general question, i.e. tell me about your research and methodology, and from there could ask questions listed in the interview guide as the conversation began to flow. Because there is not a rigorous or strict order in which the questions could be asked, the science staff member had the freedom to explore the topics the Resident would ask about with better context and without constraint. 
> - Vicky Steeves, NDSR report

If you find that your subject is having trouble opening up, or not giving you much detail, you can try these cues that might help them along in providing a more elaborate response:

```{r, echo=FALSE}
probe <- data.frame(
   Probe = c("Silent", "Echo", "Neutral", "Direct", "Detail", "Clarifying"), 
   Method = c("Nod slowly, tilt head","Repeat last statement and ask the participant to continue","'I see' or 'uh huh'","'Tell me more'", "Who, where, what, when, how questions", "'You said 'x', could you describe what you mean by that?'")
)
```

```{r, echo=FALSE}
knitr::kable(probe, booktabs = TRUE, escape = TRUE)
```

Table 2. Types of probes and their corresponding methods. [@nixon]

> Some staff took their cues from me, others waited for each question or topic, and others just gave me a complete runthrough without much prompting or other questions necessary. The Resident was responsible for understanding the body language and general emotional output of the interviewee, and steering the conversation appropriately. 
> - Vicky Steeves, NDSR report

<br>

<img src="imgs/exercise.png" style="float:left; padding-right: 0.5em;" alt="exercise">  **EXERCISE 3 -- 15 minutes. Interview each other using the questions you developed in the last exercise, taking notes during the interview (no audio recording!).**

### Post-interview {-}

Immediately after the video, you should take the following steps to make sure that you capture all the information correctly, through the recordings and notes but also through your observations:

+ Verify the recording device worked throughout the interview
+ Make any notes on your written notes, e.g., to clarify any scratchings, ensure pages are numbered, fill out any notes that don't make senses, etc.
+ Write down any observations made during the interview. For example, was the respondent particularly nervous at any time? Were there any surprises during the interview? How did the interviewee understand and react to the questions?
+ How well do you think you asked the questions?
+ How was the rapport?
+ What ideas emerged from the interview?
+ Were some questions rendered irrelevant by others?

[@macnamara, @nixon]