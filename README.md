# NDSR Art Immersion Week 2018

[![forthebadge](https://forthebadge.com/images/badges/cc-by.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/certified-steve-bruhle.svg)](https://forthebadge.com)

## About
[The National Digital Stewardship Residency (NDSR) Art](http://ndsr-pma.arlisna.org/) program helps art and cultural institutions tackle issues of digital stewardship. It is an iteration of the NDSR program that began in 2013, with a pilot project developed by the Library of Congress in conjunction with the Institute of Museum and Library Services (IMLS). The mission of the NDSR program is to build a dedicated community of professionals who will advance our nation’s capabilities in managing, preserving, and making accessible the digital record of human achievement. NDSR Art adapts and expands the NDSR model by addressing issues of digital preservation and stewardship in relation to the arts, with a particular focus on new media and arts information. The program will support two nationally dispersed cohorts– each consisting of four recent postgraduates placed in host institutions for twelve-month residencies. The first cohort begins late July 2017 with an Immersion Week held at the Philadelphia Museum of Art.
 
 This repository holds the materials for a portion of NDSR Art's immersion week, focusing on interviewing tips, tools, and best practices for understanding direct stakeholders' needs and how to meet them. 

## Build our book locally
1. Fork, clone or download this project (See #1-4 in the 'Contribute!' section below)
2. Install R & RStudio
3. Install the bookdown, RMarkdown, and tinytex packages in RStudio with the following two commands in the R terminal:
	* `install.packages(c("rmarkdown", "bookdownplus", "tinytex"))`
	* `tinytex::install_tinytex()`
	You can also click Tools > Install Packages and type the package names (make sure "install dependencies" is checked) separated by commas.
4. Go to the project folder and click `ndsr-art.Rproj`
5. Run this command in the R terminal: `bookdown::render_book('index.Rmd', 'all')`
6. Go to the folder `_book` in the project folder and click `index.html` to view the book locally in your browser.

## Contribute!
If you'd like to contribute, that would be amazing! I will try my best to be on top of merge requests and issues. Beforey you start, please refer to our [contributing guide](CONTRIBUTING.md). 

## Contact info
You are welcome to email me at [vicky dot steeves at pratt dot edu](mailto:vicky.steeves@nyu.edu) if you have questions or concerns, or raise an issue on this repository and I will do my best to respond quickly!
